import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import './App.css';
import Search from './components/Search';
class App extends Component {
  render() {
    const store = createStore(reducers, {}, applyMiddleware());
    return (
      <Provider store={store}>
        <Search />
      </Provider>
    );
  }
}

export default App;
