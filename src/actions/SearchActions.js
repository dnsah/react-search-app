import {
  SEARCH_TERM_CHANGED
} from '../actions/types';

import {
  SITES_JSON,
  CATEGORIES_JSON
 } from '../constants';

export const searchTermChanged = (searchTerm) => {

  const searchKeys = searchTerm.split(',').map((key)=>key.trim().toLowerCase());

  const categoriesMap = CATEGORIES_JSON.reduce((map, category) => {
    map[category.id] = category;
    return map;
  }, {});

  const results = SITES_JSON.filter(result => {
    const siteName = result.siteName.toLowerCase();
  const foundByName = searchKeys.indexOf(siteName) > -1;
  if(foundByName)
    return true;

  let foundByCategory;
  result.categoryIds.forEach(catId => {
      const category = categoriesMap[catId];
      const desc = category.description.toLowerCase();
      if(searchKeys.indexOf(desc) > -1) {
        foundByCategory = true;
      }
  });

  return foundByCategory;
});

  return {
    type: SEARCH_TERM_CHANGED,
    payload: { results, searchTerm }
  };
}
