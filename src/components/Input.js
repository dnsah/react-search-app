import React, { Component } from 'react';

class Input extends Component {
  render() {
    const { text, onTextChanged, placeholder } = this.props;
    return (
      <div className="search-input-wrapper">
        <input placeholder={placeholder} value={text} onChange={(evt)=>onTextChanged(evt.target.value)} />
        <span className="fa fa-search"></span>
      </div>
    );
  }
}

export default Input;
