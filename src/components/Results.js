import React, { Component } from 'react';

class Results extends Component {
  renderResults() {
      return this.props.results.map((result, index) => {
          return (
              <div className='search-results' key={index}>
                <span><a href={result.siteUrl}>{result.siteUrl}</a></span><br />
                <span>{result.description}</span>
              </div>
            );
          }
        );
    }

  render() {
    return (
      <div className='results'>
          {this.renderResults()}
      </div>
    );
  }
}

export default Results;
