import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchTermChanged } from '../actions';
import Input from './Input';
import Results from './Results';
import {
  INPPUT_PLACE_HOLDER_TEXT,
  RESULTS_NOT_FOUND_TEXT
 } from '../constants';

class Search extends Component {

  renderResults() {
    if(this.props.searchTerm !== '' && !this.props.results.length) {
      return <div className='results-not-found'>{RESULTS_NOT_FOUND_TEXT}</div>;
    }
    return <Results results={this.props.results}/>;
  }

  render() {
    return (
      <div className='app-container'>
        <div className='header'><span>Adslotmedia</span><button className='header-close-btn'>x</button></div>
        <div className='content'>
          <Input value={this.props.searchTerm} onTextChanged={(value)=>this.props.searchTermChanged(value)} placeholder={INPPUT_PLACE_HOLDER_TEXT}/>
          {this.renderResults()}
        </div>
        <div className='footer'>
          <div style={{paddingLeft: '20%', paddingRight: '20%'}}>
            <div style={{width: '100%', borderBottom: '1px solid #948383', marginBottom: '10px'}}><span style={{color: '#FFF', lineHeight: '40px'}}>Adslotmedia</span></div>
            <span style={{color: '#fff', opacity: '0.7', fontSize: '11pt'}}>&copy;2017 Adslot</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ search }) => {
  const { searchTerm, results } = search;
  return { searchTerm, results };
};

export default connect(mapStateToProps, { searchTermChanged })(Search);
