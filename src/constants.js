export const INPPUT_PLACE_HOLDER_TEXT = 'Search Publishers';
export const RESULTS_NOT_FOUND_TEXT = `We currently don't have any results for your search, try another.`;
export const SITES_JSON =[
  {
    "id": 1,
    "siteName": "SurferMag",
    "siteUrl": "www.surfermag.com",
    "description": "This is the description for SurferMag",
    "categoryIds": [
      2
    ]
  },
  {
    "id": 2,
    "siteName": "Ebay",
    "siteUrl": "www.ebay.com.au",
    "description": "This is the description for ebay",
    "categoryIds": [
      1
    ]
  },
  {
    "id": 3,
    "siteName": "Robs UI Tips",
    "siteUrl": "www.awesomeui.com.au",
    "description": "This is the description for the best site in the world. It is the best:)",
    "categoryIds": [
      4, 3
    ]
  },
  {
    "id": 4,
    "siteName": "Table Tennis Tips - How to not come runners up",
    "siteUrl": "www.ttt.com",
    "description": "This is the description for Table Tennis Tips",
    "categoryIds": [
      1, 2, 3, 4
     ]
  }
];

export const CATEGORIES_JSON = [
  {
    id: 1,
    description: "Arts & Entertainment"
  },
  {
    id: 2,
    description: "Automotive"
  },
  {
    id: 3,
    description: "Business"
  },
  {
    id: 4,
    description: "Careers"
  }
];
