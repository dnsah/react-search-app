import {
  SEARCH_TERM_CHANGED
} from '../actions/types';

const INITIAL_STATE = {
  searchTerm: '',
  results: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SEARCH_TERM_CHANGED:
      return {...state, searchTerm: action.payload.searchTerm, results: action.payload.results }
    default:
      return state;
  }

}
